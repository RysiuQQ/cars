package com.app.service;

import com.app.model.Car;
import com.app.model.CarColor;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public interface CarService {
    List<Car> sort(final Comparator<Car> comparator);
    List<Car> getCarsBySpeedRange(final int minSpeed, final int maxSpeed);
    Map<CarColor, List<Car>> groupByColor(final CarColor color);
}
