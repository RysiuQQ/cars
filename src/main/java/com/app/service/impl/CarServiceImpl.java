package com.app.service.impl;

import com.app.model.Car;
import com.app.model.CarColor;
import com.app.service.CarService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    @UtilityClass
    static final class ErrorMessages {
        static final String COMPARATOR_NULL = "Comparator must not be null";
        static final String SPEED_POSITIVE = "Speed must be positive";
        static final String SPEED_RANGE_INCORRECT = "Min speed must be lower than max speed!";
    }

    private final List<Car> cars;

    @Override
    public List<Car> sort(final Comparator<Car> comparator) {
        if (Objects.isNull(comparator)) throw new IllegalArgumentException(ErrorMessages.COMPARATOR_NULL);

        return cars.stream().sorted(comparator).toList();
    }

    @Override
    public List<Car> getCarsBySpeedRange(final int minSpeed, final int maxSpeed) {
        if (minSpeed <= 0 || maxSpeed <= 0) throw new IllegalArgumentException(ErrorMessages.SPEED_POSITIVE);
        if (minSpeed >= maxSpeed) throw new IllegalArgumentException(ErrorMessages.SPEED_RANGE_INCORRECT);

        return cars.stream().filter(car -> (car.getSpeed() >= minSpeed && car.getSpeed() <= maxSpeed)).toList();
    }

    @Override
    public Map<CarColor, List<Car>> groupByColor(final CarColor color) {
        return cars.stream().collect(Collectors.groupingBy(Car::getColor));
    }
}