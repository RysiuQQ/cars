package com.app.validation.impl;

import com.app.data.model.CarData;
import com.app.logs.Logging;
import com.app.validation.Validator;
import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Objects;

import static com.app.logs.Logging.*;

@Component
@RequiredArgsConstructor
public class CarDataValidator implements Validator<CarData> {

    @UtilityClass
    static final class ErrorMessages {
        static final String REGEX_DOES_NOT_MATCH = "Regex does not match!";
        static final String SPEED_POSITIVE = "Speed must be positive!";
        static final String PRICE_POSITIVE = "Price must be positive";
    }

    private boolean isValid = true;

    @Override
    public boolean validate(final CarData carData) {
        if (doesNotMatchRegex(carData.model()) || doesNotMatchRegex(carData.make()) ||
                carData.equipment().stream().anyMatch(eq -> !doesNotMatchRegex(eq.toString()))) {
            isValid = false;
            logger.error(ErrorMessages.REGEX_DOES_NOT_MATCH);
        }
        if (carData.speed() <= 0) {
            isValid = false;
            logger.error(ErrorMessages.SPEED_POSITIVE);
        }
        if (carData.price().compareTo(BigDecimal.ZERO) <= 0) {
            isValid = false;
            logger.error(ErrorMessages.PRICE_POSITIVE);
        }

        return isValid;
    }

    private static boolean doesNotMatchRegex(final String text) {
        final String regex = "^[A-Z0-9\\s]+$";

        return Objects.isNull(text) || !text.matches(regex);
    }
}
