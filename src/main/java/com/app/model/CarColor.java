package com.app.model;

public enum CarColor {
    BLACK,
    WHITE,
    BLUE,
    GREEN,
    RED,
    YELLOW,
    PURPLE
}
