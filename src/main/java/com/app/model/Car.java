package com.app.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Set;

@Builder
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Car {

    @EqualsAndHashCode.Include
    private long id;
    private String make;
    private String model;
    private final int speed;
    private final CarColor color;
    private final BigDecimal price;
    private final Set<CarEquipment> equipment;
}
