package com.app.model;

public enum CarEquipment {
    AIR_CONDITIONING,
    BLUETOOTH,
    ABS,
    SPARE_TIRE
}
