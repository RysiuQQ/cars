package com.app.model;

import java.util.Comparator;

public interface CarComparator {
    Comparator<Car> byMake = Comparator.comparing(Car::getMake);
    Comparator<Car> byModel = Comparator.comparing(Car::getMake);
    Comparator<Car> byPrice = Comparator.comparing(Car::getPrice);
}
