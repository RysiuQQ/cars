package com.app.data.model;

import com.app.model.Car;
import com.app.model.CarColor;
import com.app.model.CarEquipment;

import java.math.BigDecimal;
import java.util.Set;

public record CarData(
        long id,
        String make,
        String model,
        int speed,
        CarColor color,
        BigDecimal price,
        Set<CarEquipment> equipment
) {
    public Car toCar(final CarData carData) {
        return Car.builder()
                .id(carData.id())
                .make(carData.make())
                .model(carData.model())
                .speed(carData.speed())
                .color(carData.color())
                .price(carData.price())
                .equipment(carData.equipment())
                .build();
    }
}
