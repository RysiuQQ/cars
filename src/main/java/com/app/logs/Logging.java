package com.app.logs;

import lombok.experimental.UtilityClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@UtilityClass
public class Logging {
    public static final Logger logger = LoggerFactory.getLogger(Logging.class);
}
